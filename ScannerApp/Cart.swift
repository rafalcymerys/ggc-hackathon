//
//  Cart.swift
//  ScannerApp
//
//  Created by Rafal Cymerys on 4/7/18.
//  Copyright © 2018 Rafal Cymerys. All rights reserved.
//

import UIKit

class Cart: NSObject {

    static let sharedInstance = Cart()
    
    var itemCodes: [String] = [String]()
    var items: [Product] = [Product]()
    
    func size() -> Int {
        return items.count
    }
    
    func totalPrice() -> Double {
        return items.reduce(0.0, { (sum: Double, product: Product) -> Double in
            return sum + product.price
        })
    }
    
    func addItem(itemCode: String) {
        if !itemCodes.contains(itemCode) {
            itemCodes.append(itemCode)
            items.append(Product.available["ASDF"]!)
        }
    }
    
}
