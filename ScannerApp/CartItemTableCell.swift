//
//  CartItemTableCell.swift
//  ScannerApp
//
//  Created by Rafal Cymerys on 4/7/18.
//  Copyright © 2018 Rafal Cymerys. All rights reserved.
//

import UIKit

class CartItemTableCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var photoView: UIImageView!
    
}
