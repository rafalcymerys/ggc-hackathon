//
//  Product.swift
//  ScannerApp
//
//  Created by Rafal Cymerys on 4/7/18.
//  Copyright © 2018 Rafal Cymerys. All rights reserved.
//

import UIKit

class Product: NSObject {
    
    let name: String
    let price: Double
    let photoName: String
    
    init(name: String, price: Double, photoName: String) {
        self.name = name
        self.price = price
        self.photoName = photoName
    }
    
    static let available: [String:Product] = [
        "ASDF": Product(name: "Czarna Sukienka", price: 19.99, photoName: "Sukienka")
    ]
    
}
